package februus_client

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"gitlab.com/sidmal/februus-client/gen/februus"
)

type FebruusClient struct {
	conn *grpc.ClientConn
	cl   februus.FebruusClient
}

func NewFebruusClient(address string) (*FebruusClient, error) {
	conn, err := grpc.NewClient(address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("can't connect to grpc host %s: %v", address, err)
	}

	instance := &FebruusClient{
		conn: conn,
		cl:   februus.NewFebruusClient(conn),
	}

	return instance, nil
}

func (m *FebruusClient) CheckToken(ctx context.Context, token string) (*februus.CheckTokenResponse, error) {
	req := &februus.CheckTokenRequest{
		Token: token,
	}
	rsp, err := m.cl.CheckToken(ctx, req)
	if err != nil {
		return nil, fmt.Errorf("error occured during the token verification process: %v", err)
	}

	return rsp, nil
}

func (m *FebruusClient) CreateProject(ctx context.Context, id, name string) error {
	req := &februus.CreateProjectRequest{
		Id:   id,
		Name: name,
	}
	_, err := m.cl.CreateProject(ctx, req)
	if err != nil {
		return fmt.Errorf("error occured during the project creation: %v", err)
	}

	return nil
}

func (m *FebruusClient) ChangeProjectStatus(ctx context.Context, id string, status februus.ProjectStatus) error {
	req := &februus.ChangeProjectStatusRequest{
		Id:     id,
		Status: status,
	}
	_, err := m.cl.ChangeProjectStatus(ctx, req)
	if err != nil {
		return fmt.Errorf("error occured during the project change status: %v", err)
	}

	return nil
}

func (m *FebruusClient) DeleteProject(ctx context.Context, id string) error {
	req := &februus.DeleteProjectRequest{
		Id: id,
	}
	_, err := m.cl.DeleteProject(ctx, req)
	if err != nil {
		return fmt.Errorf("error occured during the project deletion: %v", err)
	}

	return nil
}

func (m *FebruusClient) ListingUsers(
	ctx context.Context,
	req *februus.ListUsersRequest) (*februus.ListUsersResponse, error) {
	res, err := m.cl.ListingUsers(ctx, req)
	if err != nil {
		return nil, fmt.Errorf("error occured during the user listing: %v", err)
	}

	return res, nil
}

func (m *FebruusClient) CreateUser(
	ctx context.Context,
	projectID, email, firstName, lastName string,
	withEmptyPassword, withoutEmailSending bool,
) error {
	req := &februus.CreateUserRequest{
		ProjectId:           projectID,
		Email:               email,
		FirstName:           firstName,
		LastName:            lastName,
		WithEmptyPassword:   withEmptyPassword,
		WithoutEmailSending: withoutEmailSending,
	}
	_, err := m.cl.CreateUser(ctx, req)
	if err != nil {
		return fmt.Errorf("error occured during the user creation: %v", err)
	}

	return nil
}

func (m *FebruusClient) DeleteUser(ctx context.Context, uuid string) error {
	req := &februus.DeleteUserRequest{
		Uuid: uuid,
	}
	_, err := m.cl.DeleteUser(ctx, req)
	if err != nil {
		return fmt.Errorf("error occured during the user deletion: %v", err)
	}

	return nil
}

func (m *FebruusClient) EnableOtp(ctx context.Context, email string) (*februus.EnableOtpResponse, error) {
	req := &februus.EnableOtpRequest{
		UserEmail: email,
	}
	res, err := m.cl.EnableOtp(ctx, req)
	if err != nil {
		return nil, fmt.Errorf("unable to enable auth throws otp code: %v", err)
	}

	return res, nil
}

func (m *FebruusClient) VerifyOTP(ctx context.Context, email, code string) error {
	req := &februus.VerifyOTPRequest{
		UserEmail: email,
		Code:      code,
	}
	_, err := m.cl.VerifyOTP(ctx, req)
	if err != nil {
		return fmt.Errorf("otp code verification failed: %v", err)
	}

	return nil
}

func (m *FebruusClient) ChangePassword(ctx context.Context, email, currentPwd, pwd, repeatPwd string) error {
	req := &februus.ChangePasswordRequest{
		UserEmail:         email,
		CurrentPassword:   currentPwd,
		NewPassword:       pwd,
		NewPasswordRepeat: repeatPwd,
	}
	_, err := m.cl.ChangePassword(ctx, req)
	if err != nil {
		return fmt.Errorf("otp code verification failed: %v", err)
	}

	return nil
}

func (m *FebruusClient) Close() error {
	return m.conn.Close()
}

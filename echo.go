package februus_client

import (
	"net/http"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/sidmal/februus-client/gen/februus"
)

type FebruusAuthContext struct {
	echo.Context
	User *februus.CheckTokenResponse
}

func FebruusAuthMiddleware(cl *FebruusClient) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			token := ctx.Request().Header.Get("Authorization")
			if token == "" {
				return echo.NewHTTPError(http.StatusUnauthorized, "Authorization required")
			}

			extractedToken := strings.Split(token, "Bearer ")
			if len(extractedToken) != 2 || extractedToken[1] == "" {
				return echo.NewHTTPError(http.StatusUnauthorized, "Authorization required")
			}

			res, err := cl.CheckToken(ctx.Request().Context(), extractedToken[1])
			if err != nil {
				return echo.NewHTTPError(http.StatusUnauthorized, "Authorization required")
			}

			februusAuthContext := FebruusAuthContext{
				Context: ctx,
				User:    res,
			}

			return next(februusAuthContext)
		}
	}
}

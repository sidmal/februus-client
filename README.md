# Februus authorisation client

```shell
protoc -I ./proto ./proto/februus.proto --go_out=./gen/februus/ --go_opt=paths=source_relative --go-grpc_out=./gen/februus/ --go-grpc_opt=paths=source_relative
```